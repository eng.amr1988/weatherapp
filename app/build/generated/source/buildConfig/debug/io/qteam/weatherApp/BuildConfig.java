/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.qteam.weatherApp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "io.qteam.weatherApp";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from build type: debug
  public static final String HOST = "https://api.openweathermap.org/data/2.5/";
}
