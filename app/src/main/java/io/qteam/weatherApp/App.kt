package io.qteam.weatherApp

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.google.android.gms.security.ProviderInstaller
import io.qteam.weatherApp.BuildConfig.HOST
import io.qteam.weatherApp.di.appModule
import io.qteam.weatherApp.network.retrofit.APInterceptor
import io.qteam.weatherApp.network.retrofit.Service
import okhttp3.*
import org.koin.android.ext.android.startKoin
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        createApi(APInterceptor(), this)
        startKoin(this, listOf(appModule))
        MultiDex.install(this)

    }


    companion object {

        lateinit var getService: Service


        internal fun createApi(
            apiInterceptur: APInterceptor?,
            context: Context
        ) {
            val clientBuilder: OkHttpClient.Builder
            val client: OkHttpClient
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            clientBuilder = OkHttpClient.Builder()
                .addInterceptor(interceptor)

            apiInterceptur?.let {
                clientBuilder.addInterceptor(
                    apiInterceptur
                )
            }



            ProviderInstaller.installIfNeeded(context)
            client = clientBuilder.build()


            val retrofit = Retrofit.Builder()
                .baseUrl(HOST)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            getService = retrofit.create(Service::class.java)
        }
    }
}
