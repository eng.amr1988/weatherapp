package io.qteam.weatherApp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Models {
    data class WeatherModel(
        @SerializedName("daily") val dailyList: MutableList<DailyWeather>
    ) : Serializable

    data class DailyWeather(
        @SerializedName("dt") val date: Long,
        @SerializedName("temp") val temp: Temprature
    ) : Serializable

    data class Temprature(
        @SerializedName("min") val min: Float,
        @SerializedName("max") val max: Float
    ) : Serializable
}