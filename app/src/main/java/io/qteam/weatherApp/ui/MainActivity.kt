package io.qteam.weatherApp.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.qteam.weatherApp.R
import io.qteam.weatherApp.ui.base.BaseActivity

class MainActivity : BaseActivity() {
    override fun buildIntent(context: Context, data: Any?): Intent {
        return Intent(this, MainActivity::class.java)
    }

    override fun getActivityView(): Int {
        return R.layout.activity_main
    }

    override fun afterInflation(savedInstance: Bundle?) {

    }
}