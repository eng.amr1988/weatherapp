package io.qteam.weatherApp.ui.base


interface DialogViewCallBack : BaseViewCallBack {

    fun dismissDialog(tag: String)
}
