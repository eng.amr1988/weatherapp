package io.qteam.weatherApp.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.qteam.weatherApp.R
import io.qteam.weatherApp.ui.base.BaseActivity

class SplashActivity : BaseActivity() {
    override fun buildIntent(context: Context, data: Any?): Intent {
        return Intent(this, SplashActivity::class.java)
    }

    override fun getActivityView(): Int {
        return R.layout.activity_splash
    }

    override fun afterInflation(savedInstance: Bundle?) {

    }
}