package  io.qteam.weatherApp.network


import io.qteam.weatherApp.App
import io.qteam.weatherApp.model.GenericResponse
import io.qteam.weatherApp.model.Models
import io.qteam.weatherApp.network.retrofit.Service
import io.reactivex.Observable

class AppRepository : Service {
    override fun getWeather(lang: String): Observable<GenericResponse<Models.WeatherModel>> {
        return App.getService.getWeather(lang)
    }
}
