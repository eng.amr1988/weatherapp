package  io.qteam.weatherApp.network.retrofit

import io.qteam.weatherApp.model.GenericResponse
import io.qteam.weatherApp.model.Models
import io.reactivex.Observable
import retrofit2.http.*


interface Service {

    @GET("onecall")
    fun getWeather(@Query("lang") lang: String): Observable<GenericResponse<Models.WeatherModel>>


}