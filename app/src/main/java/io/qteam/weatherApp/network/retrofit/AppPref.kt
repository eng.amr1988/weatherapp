package  io.qteam.weatherApp.network.retrofit

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.text.TextUtils
import com.google.gson.Gson
import io.qteam.weatherApp.App
import io.qteam.weatherApp.model.Models


class AppPref(context: Context) {

    private var prefs: SharedPreferences? = null
    var accessToken: String? = ""

    init {
        try {
            prefs =
                context.applicationContext.getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE)
            accessToken = prefs!!.getString(KEY_ACCESS_TOKEN, null)
        } catch (e: Exception) {
        }
    }

    fun setAccessToken(context: Context, accessToken: String) {
        try {
            if (!TextUtils.isEmpty(accessToken)) {
                this.accessToken = accessToken
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    prefs!!.edit().putString(KEY_ACCESS_TOKEN, accessToken).commit()
                }

            }

        } catch (e: Exception) {
        }
    }

    
    fun saveNotificationToken(token: String?) {
        if (!TextUtils.isEmpty(token)) {
            prefs?.edit()?.putString(KEY_NOTIFICATION_ACCESS_TOKEN, token)?.commit()
        }
    }

    
    fun logout() {
        val notificationToken = getNotificationToken()
        prefs?.edit()?.clear()?.commit()
        saveNotificationToken(notificationToken)
    }

    fun getNotificationToken(): String? {
        return prefs?.getString(KEY_NOTIFICATION_ACCESS_TOKEN, null)
    }

    
    fun removeAccessToken(context: Context) {
        try {
            this.accessToken = null
            prefs?.edit()?.putString(KEY_ACCESS_TOKEN, null)?.commit()
            removeSecureConnection(context)
        } catch (e: Exception) {
        }
    }



    fun removeSecureConnection(context: Context) {
        App.createApi(APInterceptor(),context)
    }

    companion object {
        private val KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"
        private val LOGIN_PREF = "LOGIN_PREF"
        private val KEY_NOTIFICATION_ACCESS_TOKEN = "KEY_NOTIFICATION_ACCESS_TOKEN"
    }


    fun saveTheme(color: String) {
        prefs?.edit()?.putString("theme_color", color)?.commit()
    }

    fun getTheme(): String {
        return prefs?.getString("theme_color", "blue")!!
    }



    fun saveLang(lang: String) {
        prefs?.edit()?.putString("lang", lang)?.commit()
    }

    fun getLang(): String {
        return prefs?.getString("lang", "ar")!!
    }
}
