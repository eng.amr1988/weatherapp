package  io.qteam.weatherApp.network.retrofit

import com.google.gson.Gson
import io.qteam.weatherApp.model.GenericResponse
import io.reactivex.observers.DisposableObserver
import okhttp3.ResponseBody
import retrofit2.HttpException

abstract class CallbackWrapper<T : Any> : DisposableObserver<T>() {

    protected abstract fun onSuccess(t: T)
    protected abstract fun onFail(t: String?)

    override fun onNext(t: T) {
        try {
            onSuccess(t)
        } catch (e: Exception) {
            onFail(e.message)
        }
    }

    override fun onError(e: Throwable) {
        if (e is HttpException) {
            val responseBody = e.response().errorBody()
            //  (responseBody?.string())
            onFail(getErrorMessage(responseBody!!))
        } else {
            onFail(e.message)
        }

//        else if (e is SocketTimeoutException) {
//            view.onTimeout()
//        } else if (e is IOException) {
//            view.onNetworkError()
//        } else {
//            view.onUnknownError(e.message)
//        }

    }

    override fun onComplete() {

    }

    private fun getErrorMessage(responseBody: ResponseBody): String? {
        var response: String = String(responseBody.string().toByteArray())

        var message: String?
        try {
            val error = Gson().fromJson(response, GenericResponse::class.java)
            message = error.message
        } catch (e: Exception) {
            try {
                val error = Gson().fromJson(response, GenericResponse::class.java)
                message = error.message
            } catch (e: Exception) {
                return null
            }
        }
        return message
    }
}