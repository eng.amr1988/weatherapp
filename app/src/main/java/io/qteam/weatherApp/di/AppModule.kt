package io.qteam.weatherApp.di


import io.qteam.weatherApp.network.AppRepository
import io.qteam.weatherApp.network.retrofit.AppPref
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module


val appModule = module {

    //repos
    factory { AppRepository() }
    factory { AppPref(androidContext()) }


    //viewmodel
//    viewModel { GeneralVM(get()) }


}